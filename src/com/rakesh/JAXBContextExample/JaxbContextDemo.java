package com.rakesh.JAXBContextExample;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class JaxbContextDemo {
	
	public void writeObjectToFile(String fileName, String sw) throws IOException {
		FileWriter fw = new FileWriter(fileName);
		fw.write(sw);
		fw.close();
		
	}

	public static void main(String[] args) throws JAXBException, IOException {
		JaxbContextDemo obj = new JaxbContextDemo();
		List<String> address = new ArrayList<>();
		address.add("Dwarahat");
		address.add("Delhi");
		Employee emp = new Employee(1, "Rakesh", address);
		JAXBContext jaxbContext = JAXBContext.newInstance(Employee.class); 
		Marshaller marshallerObj = jaxbContext.createMarshaller();  
	    marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);  
	    StringWriter sw = new StringWriter();
	   
	    String fileName = "MVSC"
	    		+"_"
	    		+emp.getName()
	    		+"-"
	    		+"_"
	    		+DateTimeFormatter.ofPattern("yyyyMMDDHHmmssSS").format(LocalDateTime.now())
	    		+".xml";
	      
	    marshallerObj.marshal(emp, sw);
	    
	    obj.writeObjectToFile(fileName, sw.toString());
	    System.out.println(sw.toString());
	}

}
