package com.rakesh.polymorphism;

class Derived{
	public void getDetails(String name) {
		System.out.println("The name in derived class is : "+name);
	}
}

class Test extends Derived{
	public int getDetails(String name) {
		System.out.println("The name in test class is : "+name);
}
}
public class OverridingDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
