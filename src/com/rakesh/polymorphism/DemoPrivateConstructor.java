package com.rakesh.polymorphism;

class A1{
	static String s ="";
	protected A1() {
		s += "A1";
	}
}

class B extends A1{
	private B() {
		s += "B";
	}
}

public class DemoPrivateConstructor extends A1{
	private DemoPrivateConstructor() {
		s += "C";
	}
	public static void main(String[] args) {
		new DemoPrivateConstructor();
		System.out.println(s);

	}

}
