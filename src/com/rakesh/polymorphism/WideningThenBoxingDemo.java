package com.rakesh.polymorphism;

public class WideningThenBoxingDemo {

	public void show(Long l) {
		System.out.println("Long");
	}
	
	public void show(Number n) {
		System.out.println("Number");
	}
	
	
	public static void main(String[] args) {
	new WideningThenBoxingDemo().show(3);	

	}

}
