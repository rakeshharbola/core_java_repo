package com.rakesh.polymorphism;

class A{
	
	public A(String s) {
		System.out.println("A");
	}	
}

public class Demo extends A{
	
	public Demo(String s) {
		//super(s);
		System.out.println("Demo");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
