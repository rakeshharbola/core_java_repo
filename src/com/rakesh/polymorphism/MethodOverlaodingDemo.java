package com.rakesh.polymorphism;

public class MethodOverlaodingDemo {
	
	public void show(int a, Integer b, Integer c) {
		System.out.println("Inside 1st Method");
	}
	
	public void show(int a, int b, Integer c) {
		System.out.println("Inside 2nd Method");
	}
	
	public void show(Integer a, Integer b, Integer c) {
		System.out.println("Inside 3rd Method");
	}
	
	public void show(int a, int b, int c) {
		System.out.println("Inside 4th Method");
	}
	
	public void dis(Number num) {
		System.out.println("Number");
	}
	
	public void dis(Long num) {
		System.out.println("Long");
	}
	
	public static void main(String []args) {
		new MethodOverlaodingDemo().show(1, 2, 3);
		new MethodOverlaodingDemo().dis(1);
	}
	
	

}
