package com.rakesh;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ArrayGroupingByDemo {

	public static void main(String[] args) {
		List<Integer> intList = Arrays.asList(10,20,10,40,50,40,50);
		Map<Object, Long> output = intList.stream()
				.collect(Collectors.groupingBy(e -> e, Collectors.counting()));
		
		output.forEach((k,v) -> v=v-(Integer)k);
		
		System.out.println(output);
		

	}

}
