package com.rakesh.others;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class ObjectMapperDemo {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		String jsonCarArray = 
				  "[{ \"color\" : \"Black\", \"type\" : \"BMW\" }, { \"color\" : \"Red\", \"type\" : \"FIAT\" }]";
		ObjectMapper objectMapper = new ObjectMapper();
		List<Map<String, Object>> map = objectMapper.readValue("attributes", new TypeReference<List<Map<String,Object>>>(){});
		
		System.out.println(map.get(0));

	}

}
