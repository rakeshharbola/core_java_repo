package com.rakesh.others;

public class ArraysMergeDemo {
	public static void main(String []args) {
		int []arr = {2,3,5,4};
		int []arr2 = {4,6,8,7};
		 int []result = new int[arr.length+arr2.length];
		 System.arraycopy(arr, 0, result, 0, arr.length);
		 System.arraycopy(arr2, 0, result, arr.length-1, arr2.length);
		 for(int i : result)
			 System.out.println(i);
	}
}
