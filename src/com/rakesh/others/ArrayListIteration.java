package com.rakesh.others;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArrayListIteration {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		
	/*	for(int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		int count=list.size();
		int i=0;
		while(i<count) {
			System.out.println(list.get(i));
			i++;
		}
		for(Integer num : list) {
			System.out.println(num);
		}*/
		
		
		Iterator<Integer> it = list.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}

	}

}
