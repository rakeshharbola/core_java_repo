package com.rakesh.others;

import java.util.Scanner;

public class PrimeNumberDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a no : ");
		int num = sc.nextInt();
		sc.close();
		int i=2;
		boolean isPrime=false;
		while(i<=num/2) {
			if(num%i != 0) {
				isPrime = true;
				break;
			}else {
				isPrime = false;
			}
			i++;
		}
		if(isPrime) {
			System.out.println("No is  Prime");
		}else {
			System.out.println("no is not Prime");
		}
	}

}
