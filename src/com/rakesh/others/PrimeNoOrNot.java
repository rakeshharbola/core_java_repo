package com.rakesh.others;

import java.util.Scanner;

public class PrimeNoOrNot {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a no : ");
		int num = sc.nextInt();
		sc.close();
		boolean isPrime = false;
		for(int i=2; i<=num/2; i++) {
			if(num%i==0) {
				isPrime = true;
				break;
			}
		}
		if(isPrime)
			System.out.println("Entered no is not prime.");
		else 
			System.out.println("Entered no is prime.");

	}

}
