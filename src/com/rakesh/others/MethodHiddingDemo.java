package com.rakesh.others;

class Super{
	
	public static void show() {
		System.out.println("Parent class show method.");
	}
}

class child extends Super{
	
	public static void show() {
		System.out.println("Child class show method.");
	}
}

public class MethodHiddingDemo {
	
	public static void main(String []args) {
		Super obj = new child();
		obj.show();
	}
}
