package com.rakesh.others;

import org.codehaus.jackson.annotate.JsonProperty;

public class MyClass {
	
	@JsonProperty
	private String color;
	@JsonProperty
	private String type;
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	

}
