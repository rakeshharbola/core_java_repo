package com.rakesh.others;

import java.util.Scanner;

public class FactorialDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a  no : ");
		int num = sc.nextInt();
		sc.close();
		int result = 1;
		while(num > 1) {
			result = result*num;
			num--;
		}
		System.out.println("Factorial of above no is : "+result);
	}

}
