package com.rakesh.others;

import java.util.Scanner;

public class NoSwapWithoutThirdNo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the first no : ");
		int first = sc.nextInt();
		System.out.println("First no is : "+first);
		System.out.println("Enter the second no : ");
		int second = sc.nextInt();
		sc.close();
		System.out.println("Second no is : "+second);
		
		first = first+second;
		second = first-second;
		first = first-second;
		System.out.println("First no after swap is : "+first);
		System.out.println("Second no after swap is : "+second);
	}

}
