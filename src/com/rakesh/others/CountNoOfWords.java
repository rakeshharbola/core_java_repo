package com.rakesh.others;

import java.util.HashMap;
import java.util.Map;

public class CountNoOfWords {

	public static void main(String[] args) {
		String str = "I belong to India and India is a great country and I Love India";
		String []arr = str.split(" ");
		System.out.println("Total no of words in String are : "+arr.length);
		int val = 0;
		// Using HashMap
		
		Map<String, Integer> map = new HashMap<>();
		for(int i =0; i<arr.length; i++) {
			if(map.containsKey(arr[i])) {
				val = map.get(arr[i])+1;
			}else {
				val = 1;
			}
			map.put(arr[i], val);
		}
		
		for(Map.Entry<String, Integer> entry : map.entrySet()){
			System.out.println(entry.getKey()+" = "+entry.getValue());
		}
	}

}
