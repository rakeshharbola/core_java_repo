package com.rakesh.others;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a no : ");
		int num = sc.nextInt();
		sc.close();
		int result=0;
		int mod;
		while(num>0) {
			mod=num%10;
			result = result*10+mod;
			num=num/10;
		}
		System.out.println(result);
	}

}
