package com.rakesh.others;

import java.util.Scanner;

public class ArmstrongNoDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number : ");
		int num = sc.nextInt();
		int mod, result=0, bckup=num;
		sc.close();
		int digits=new ArmstrongNoDemo().getDigits(num);
		while(num>0) {
		    mod=num%10;
		    int temp=1;
			for(int i=0; i<digits; i++) {
				temp = temp*mod;
			}			
			result=result+temp;
			num=num/10;
		}
		if(result==bckup) {
			System.out.println("No is Armstrong.");
		}else {
			System.out.println("No is not Armstrong.");
		}
	}
	
	public int getDigits(int num) {
		int digits = 0;
		while(num>0) {
			num = num/10;
			digits++;
		}
	return digits;	
	}
}
