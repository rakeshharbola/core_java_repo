package com.rakesh.others;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class IterateHashMap {

	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();
		map.put("first", "One");
		map.put("Second", "Two");
		map.put("Third", "Three");
		int count = map.entrySet().size();
		Iterator it = map.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, String> entry = (Map.Entry)it.next();
			System.out.println(entry.getKey()+" = "+entry.getValue());
		}
		
		for(Map.Entry<String, String> entry : map.entrySet()) {
			System.out.println(entry.getKey()+" = "+entry.getValue());
		}
		
		Set<String> set = map.keySet();
		for(String key : set) {
			System.out.println(key+" = "+map.get(key));
		}
	}
}
