package com.rakesh.others;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.rakesh.stream.Person;

public class ListToMapDemo {

	public static void main(String[] args) {
		List<Person> list = Arrays.asList(
				new Person(new Integer(1), "A", new Integer(10000)), 
				new Person(new Integer(2), "B", new Integer(20000)), 
				new Person(new Integer(3), "C", new Integer(30000)), 
				new Person(new Integer(4), "D", new Integer(40000)),
				new Person(new Integer(1), "B", new Integer(10000)), 
				new Person(new Integer(3), "D", new Integer(10000)));
		
	/*	List<Person> list = new ArrayList<Person>();
		list.add(new Person(new Integer(1), "A", new Integer(10000)));
		list.add(new Person(new Integer(2), "B", new Integer(20000)));
		list.add(new Person(new Integer(3), "C", new Integer(30000)));*/
		
		Map<String, ArrayList<Person>> map = new HashMap<String, ArrayList<Person>>();
		
		Iterator<Person> it = list.iterator();
		while(it.hasNext()) {
			Person value = it.next();
			String key = value.getName();
			if(!map.containsKey(key)) {
				map.put(key, new ArrayList<Person>());
			}
			map.get(key).add(value);
		}
		
		System.out.println(map);

	}

}
