package com.rakesh.designpatterns.builder;

public class Computer {

	private long HDD;
	private long RAM;
	private boolean isGraphicEnabled;
	private boolean isBluetoothEnabled;
	
	private Computer(ComputerBuilder builder) {
		this.HDD = builder.HDD;
		this.RAM = builder.RAM;
		this.isGraphicEnabled = builder.isGraphicEnabled;
		this.isBluetoothEnabled = builder.isBluetoothEnabled;
	}

	public long getHDD() {
		return HDD;
	}

	public long getRAM() {
		return RAM;
	}

	public boolean isGraphicEnabled() {
		return isGraphicEnabled;
	}

	public boolean isBluetoothEnabled() {
		return isBluetoothEnabled;
	}
	
	
	public static class ComputerBuilder{
		
		private long HDD;
		private long RAM;
		
		private boolean isGraphicEnabled;
		private boolean isBluetoothEnabled;
		
		public ComputerBuilder(long HDD, long RAM) {
			this.HDD = HDD;
			this.RAM = RAM;
		}
		
		public ComputerBuilder setGraphicEnabled(boolean isGraphicEnabled) {
			this.isGraphicEnabled = isGraphicEnabled;
			return this;
		}
		
		public ComputerBuilder setBluetoothEnabled(boolean isBluetoothEnabled) {
			this.isBluetoothEnabled = isBluetoothEnabled;
			return this;
		}
		
		public Computer build() {
			return new Computer(this);
		}
		
		
	}
}
