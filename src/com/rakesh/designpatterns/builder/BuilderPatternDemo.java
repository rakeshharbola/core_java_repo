package com.rakesh.designpatterns.builder;

public class BuilderPatternDemo {
	
	public static void main(String []args) {
		Computer com = new Computer.ComputerBuilder(512L, 8L)
				.setGraphicEnabled(true)
				.setBluetoothEnabled(false)
				.build();
		
		System.out.println(com.getHDD());
		System.out.println(com.getRAM());
		System.out.println(com.isGraphicEnabled());
		System.out.println(com.isBluetoothEnabled());
	}

}
