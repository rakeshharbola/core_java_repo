package com.rakesh.designpatterns.factory;

public abstract class Computer {
	
	protected abstract long getHDD();
	protected abstract long getRAM();
	
	public String toString() {
		return "Ram of "+this.getClass().getSimpleName()+" is : "+this.getRAM()+" and HDD of "+this.getClass().getSimpleName()+"  is : "+this.getHDD(); 
	}
}
