package com.rakesh.designpatterns.factory;

public class ComputerFactory {

	public static Computer getComputer(String type, long HDD, long RAM) {
		if("PC".equals(type)) {
			return new PC(HDD, RAM);
		}else if ("Server".equals(type)) {
			return new Server(HDD, RAM);	
		}
		return null;			
	}
}
