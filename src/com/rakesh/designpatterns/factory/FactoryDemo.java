package com.rakesh.designpatterns.factory;

public class FactoryDemo {

	public static void main(String []args) {
		
		Computer pc = ComputerFactory.getComputer("PC", 512L, 8L);
		Computer server = ComputerFactory.getComputer("Server", 1048L, 32L);
		
		System.out.println(pc);
		System.out.println(server);
		System.out.println("Type of object pc is = "+pc.getClass().getSimpleName());
		System.out.println("Type of object server is = "+server.getClass().getSimpleName());
	}
}
