package com.rakesh.designpatterns.factory;

public class Server extends Computer {

	private long HDD;
	private long RAM;
	
	public Server(long hDD, long rAM) {
		super();
		HDD = hDD;
		RAM = rAM;
	}

	public long getHDD() {
		return HDD;
	}

	public long getRAM() {
		return RAM;
	}
	
	/*public String toString() {
		return "For Server : RAM is = "+this.getRAM()+" and HDD is = "+this.getHDD();
	}*/
}
