package com.rakesh.designpatterns.singleton;

public class ThreadSafeInitializedSingleton {
	
	private static ThreadSafeInitializedSingleton instance;
	
	private ThreadSafeInitializedSingleton() {}
	
/*	public static synchronized ThreadSafeInitializedSingleton getInstance() {
		if(instance == null) {
			instance = new ThreadSafeInitializedSingleton();
		}
		return instance;
	}*/
	
	public static ThreadSafeInitializedSingleton getInstance() {
		if(instance == null) {
			synchronized (ThreadSafeInitializedSingleton.class) {
				if(instance == null)
					instance = new ThreadSafeInitializedSingleton();
			}
		}
		return instance;
	}
	
	
	// This method is exactly same as the above method, this was just created to revise the concept.
	
	public static ThreadSafeInitializedSingleton getInstanceNew(){
		if(instance == null) {
			synchronized(ThreadSafeInitializedSingleton.class){
				if(instance == null) {
					instance = new ThreadSafeInitializedSingleton();
				}
			}
		}
		return instance;
	}
	
}
