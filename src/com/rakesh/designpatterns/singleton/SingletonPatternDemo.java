package com.rakesh.designpatterns.singleton;

public class SingletonPatternDemo {

	public static void main(String[] args) {
		
	// Eager Initialization Instances
		
		EagerInitializationDemo instance_1 =  EagerInitializationDemo.getInstance();
		EagerInitializationDemo instance_2 =  EagerInitializationDemo.getInstance();
		
		System.out.println("Hashcode of 1st Object : "+instance_1.hashCode());
		System.out.println("Hashcode of 2nd Object : "+instance_2.hashCode());
		
	// Static Block Instances
		
		StaticBlockInitializatioDemo instance_3=  StaticBlockInitializatioDemo.getInstance();
		StaticBlockInitializatioDemo instance_4 =  StaticBlockInitializatioDemo.getInstance();
		
		System.out.println("Hashcode of 1st Object : "+instance_3.hashCode());
		System.out.println("Hashcode of 2nd Object : "+instance_4.hashCode());
		
	
	// Lazy Initialized Instances
		
		LazyInitializedSingleton instance_5=  LazyInitializedSingleton.getInstance();
		LazyInitializedSingleton instance_6 =  LazyInitializedSingleton.getInstance();
		
		System.out.println("Hashcode of 1st Object : "+instance_5.hashCode());
		System.out.println("Hashcode of 2nd Object : "+instance_6.hashCode());
		
		
		
	// Thread Safe Initialized Instances
		
		ThreadSafeInitializedSingleton instance_7=  ThreadSafeInitializedSingleton.getInstance();
		ThreadSafeInitializedSingleton instance_8 =  ThreadSafeInitializedSingleton.getInstance();
		
		System.out.println("Hashcode of 1st Object : "+instance_7.hashCode());
		System.out.println("Hashcode of 2nd Object : "+instance_8.hashCode());
	
		
	// Bill Pugh Initialized Instances
		
		BillPughInitializedSingleton instance_9=  BillPughInitializedSingleton.getInstance();
		BillPughInitializedSingleton instance_10 =  BillPughInitializedSingleton.getInstance();
		
		System.out.println("Hashcode of 1st Object : "+instance_9.hashCode());
		System.out.println("Hashcode of 2nd Object : "+instance_10.hashCode());
	}

}
