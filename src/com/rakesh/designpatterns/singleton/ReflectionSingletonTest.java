package com.rakesh.designpatterns.singleton;

import java.lang.reflect.Constructor;

public class ReflectionSingletonTest {

		public static void main(String []args) {
			ThreadSafeInitializedSingleton instance_1 = ThreadSafeInitializedSingleton.getInstance();
			ThreadSafeInitializedSingleton instance_2 = null;
		
			try {
				Constructor[] constructors = ThreadSafeInitializedSingleton.class.getDeclaredConstructors();
				for(Constructor con : constructors) {
					con.setAccessible(true);
					instance_2 =  (ThreadSafeInitializedSingleton)con.newInstance();
					break;
					}
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			System.out.println(instance_1.hashCode());
			System.out.println(instance_2.hashCode());
		}
			
}
