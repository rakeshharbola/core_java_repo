package com.rakesh.designpatterns.singleton;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializationSingletonDemo{

	public static void main(String [] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		SerializableSingleton instance1 =SerializableSingleton.getInstance();
		
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("instance.ser"));
		out.writeObject(instance1);
		out.close();
		
		ObjectInputStream in = new ObjectInputStream(new FileInputStream("instance.ser"));
		SerializableSingleton instance2 = (SerializableSingleton)in.readObject();
		in.close();
		
		System.out.println("Hashcode of instance1 = "+instance1.hashCode());
		System.out.println("Hashcode of instance2 = "+instance2.hashCode());
	}
	

}

class SerializableSingleton implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private SerializableSingleton() {};
	
	public static class SerializableSingletonHelper{
		private static SerializableSingleton INSTANCE = new SerializableSingleton();
	}
	
	public static SerializableSingleton getInstance() {
		return SerializableSingletonHelper.INSTANCE;
	}
	
}