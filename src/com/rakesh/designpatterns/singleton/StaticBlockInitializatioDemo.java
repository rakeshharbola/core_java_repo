package com.rakesh.designpatterns.singleton;

public class StaticBlockInitializatioDemo {
	
	private static StaticBlockInitializatioDemo instance;
	
	static {
		try {
			instance = new StaticBlockInitializatioDemo();
		}catch(Exception e) {
			throw new RuntimeException("Exception occured during instance creation.");
		}
	}
	
	private StaticBlockInitializatioDemo() {}
	
	public static StaticBlockInitializatioDemo getInstance() {
		return instance;
	}

}
