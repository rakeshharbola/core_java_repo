package com.rakesh.designpatterns.singleton;

public class BillPughInitializedSingleton {
	
	private BillPughInitializedSingleton() {}
	
	private static class BillPughSingleton{
		private static final BillPughInitializedSingleton INSTANCE = new BillPughInitializedSingleton();
	}
	
	public static BillPughInitializedSingleton getInstance() {
		return BillPughSingleton.INSTANCE;
	}
	
	
	// Below 2 methods have same behaviour as above methods have, below methods just got created to revise the concept.
	
	public static class BilPughSingletonHelp{
		private static final BillPughInitializedSingleton INSTANCE =new BillPughInitializedSingleton(); 
	}
	
	public BillPughInitializedSingleton getInstanceNew() {
		return BilPughSingletonHelp.INSTANCE;
	}
}
