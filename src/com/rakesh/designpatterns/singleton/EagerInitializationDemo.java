package com.rakesh.designpatterns.singleton;


public class EagerInitializationDemo {

private static final EagerInitializationDemo instance = new EagerInitializationDemo();
	
	private EagerInitializationDemo() {}
	
	public static EagerInitializationDemo getInstance() {
		return instance;
	}

}
