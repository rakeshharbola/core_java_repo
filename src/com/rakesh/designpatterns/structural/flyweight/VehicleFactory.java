package com.rakesh.designpatterns.structural.flyweight;

import java.util.HashMap;

public class VehicleFactory {
	
	public static Vehicle getVehicle(String type) {
		HashMap<String, Vehicle> fact = new HashMap<>();
		Vehicle v = null;
		if(fact.containsKey(type)) {
			v = fact.get(type);
		}else {
			switch(type) {
			case "Cycle":
				System.out.println("Cycle is created.");
				v = new Cycle();
				break;
			case "Truck":
				System.out.println("Truck is created.");
				v = new Truck();
				break;
			default:
				System.out.println("The Vehicle type doesn't exist.");
					
			}
			fact.put(type, v);
		}
		return v;
	}
}
