package com.rakesh.designpatterns.structural.flyweight;

public class FlyWeight {
	
	/*This design pattern is used to share the the same properties of objects, means if few properties of any class remains are
	same for all the objects, then those properties are called intrinsic properties.
		In Flyweight Pattern, Objects are created with intrinsic peroperties and shared whereever required and can set extrinsic 
		properties according to the requirement.*/
	

	public static void main(String[] args) {
		Vehicle cycle = VehicleFactory.getVehicle("Cycle");
		cycle.assignColor("Red");
		cycle.startEngine();
		
		Vehicle truck = VehicleFactory.getVehicle("Truck");
		truck.assignColor("Blue");
		truck.startEngine();
	}

}
