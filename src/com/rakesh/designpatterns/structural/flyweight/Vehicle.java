package com.rakesh.designpatterns.structural.flyweight;

public interface Vehicle {
	
	public void assignColor(String color);
	public void startEngine();
	

}
