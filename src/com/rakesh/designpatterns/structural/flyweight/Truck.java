package com.rakesh.designpatterns.structural.flyweight;

public class Truck implements Vehicle{
	
	private final String maxSpeed;
	private String color;	

	Truck(){
		this.maxSpeed = "120 kmph";
	}
	
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public void assignColor(String color) {
		this.color = color;
	}

	@Override
	public void startEngine() {
		System.out.println(this.color+" Truck Engine is Started.");
		
	}

}
