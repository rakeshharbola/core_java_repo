package com.rakesh.designpatterns.structural.flyweight;

public class Cycle implements Vehicle{
	
	private final String maxSpeed;
	private String color;
	
	Cycle(){
		this.maxSpeed = "15 kmph";
	}
	
	public void setColor(String color) {
		this.color = color;
	}


	@Override
	public void assignColor(String color) {
		this.color = color;
		
	}

	@Override
	public void startEngine() {
		System.out.println(this.color+ " Cycle engine is started.");
		
	}

}
