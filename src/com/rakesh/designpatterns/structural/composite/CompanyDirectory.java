package com.rakesh.designpatterns.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class CompanyDirectory implements Employee{

	private List<Employee> empList = new ArrayList<>();
		
	@Override
	public void showEmpDetails() {
		for(Employee emp : empList) {
			emp.showEmpDetails();
		}
		
	}
	
	public void addEmployee(Employee emp) {
		empList.add(emp);
	}
	
	public void removeEmployee(Employee emp) {
		empList.remove(emp);
	}
	

}
