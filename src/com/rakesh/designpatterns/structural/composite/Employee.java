package com.rakesh.designpatterns.structural.composite;

public interface Employee {

	public void showEmpDetails();
}
