package com.rakesh.designpatterns.structural.composite;

public class Developer implements Employee{

	private String name;
	private int id;
	
	Developer(String name, int id){
		this.name = name;
		this.id = id;
	}
	
	@Override
	public void showEmpDetails() {
		System.out.println(id+" is the id of developer "+name);
		
	}	
}
