package com.rakesh.designpatterns.structural.composite;

public class CompositeDemo {

	public static void main(String[] args) {
		Developer dev1 = new Developer("Rakesh", 1);
		Developer dev2 = new Developer("Vidhya", 2);
		
		Manager man1 = new Manager("Man1", 5);
		Manager man2 = new Manager("Man2", 6);
		
		CompanyDirectory engDir = new CompanyDirectory();
		engDir.addEmployee(dev1);
		engDir.addEmployee(dev2);
		
		CompanyDirectory manDir = new CompanyDirectory();
		manDir.addEmployee(man1);
		manDir.addEmployee(man2);
		
		CompanyDirectory dir = new CompanyDirectory();
		dir.addEmployee(engDir);
		dir.addEmployee(manDir);
		dir.showEmpDetails();
		
	}

}
