package com.rakesh.designpatterns.structural.composite;

public class Manager implements Employee{

	private String name;
	private int id;
	
	Manager(String name, int id){
		this.name = name;
		this.id = id;		
	}
	
	@Override
	public void showEmpDetails() {
		System.out.println(id+" is the id of manager "+name);
		
	}
	
	

}
