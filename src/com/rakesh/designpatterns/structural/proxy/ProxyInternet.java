package com.rakesh.designpatterns.structural.proxy;

import java.util.ArrayList;
import java.util.List;

public class ProxyInternet implements Internet{
	
	static List<String> restSites = new ArrayList<>();
	
	static {
		restSites.add("abc.com");
		restSites.add("def.com");
		restSites.add("xyz.com");
	}

	@Override
	public void connectTo(String hostName) throws Exception {
		Internet internet = new RealInternet();
		if(restSites.contains(hostName)) {
			throw new Exception("Site is banned.");
		}
			internet.connectTo(hostName);
	}

}
