package com.rakesh.designpatterns.structural.proxy;

public interface Internet {
	
	public void connectTo(String hostName) throws Exception;
}
