package com.rakesh.Concurrent;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapDemo {

	public static void main(String[] args) {
		ConcurrentHashMap<String, String> chm = new ConcurrentHashMap<String, String>(16, 0.75f, 4);
		chm.put("Name", "Rakesh");
		chm.put("Company", "IRIS");
		
		System.out.println(chm.size());
		System.out.println(chm.get("Name"));
	
	}

}
