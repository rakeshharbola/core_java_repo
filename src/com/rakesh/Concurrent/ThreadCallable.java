package com.rakesh.Concurrent;

import java.util.concurrent.Callable;

public class ThreadCallable implements Callable<String> {
	@Override
	public String call() {
		return "Inside Thread Callable : "+Thread.currentThread().getName();
	}
}
