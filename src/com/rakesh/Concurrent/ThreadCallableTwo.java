package com.rakesh.Concurrent;

import java.util.concurrent.Callable;

public class ThreadCallableTwo implements Callable<String> {
	@Override
	public String call() {
		return "Inside Callable Thread Two : "+Thread.currentThread().getName();
	}
}
