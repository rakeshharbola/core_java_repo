package com.rakesh.Concurrent;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorFrameworkDemo {
	
	public static void main(String []args) {
		Executor exec = Executors.newFixedThreadPool(2);
		exec.execute(new ThreadOne());
		exec.execute(new ThreadTwo());
		
		ExecutorService es = Executors.newFixedThreadPool(2);
		Future<String> future = es.submit(new ThreadCallable());
		Future<String> future2 = es.submit(new ThreadCallableTwo());
		
		if(future.isDone()) {
			try {
			System.out.println(future.get());
		}catch(Exception e) {
			e.printStackTrace();
			}
		}
		

		if(future2.isDone()) {
		try {
			System.out.println(future2.get());
		}catch(Exception e) {
			e.printStackTrace();
			}
		}
	}

}
