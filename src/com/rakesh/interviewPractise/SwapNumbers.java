package com.rakesh.interviewPractise;

public class SwapNumbers {

	public static void main(String[] args) {
		int a = 5;
		int b = 6;
		
		System.out.println("Before swap a : "+a);
		System.out.println("Before swap b : "+b);
		
		b = a+b;	
		a = b-a;
		b = b-a;
		
		System.out.println("After swap a : "+a);
		System.out.println("After swap b : "+b);
		
		
		
		

	}

}
