package com.rakesh.interviewPractise;

import java.util.Scanner;

public class PalindromeDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the string :");
		String str = sc.next();
		sc.close();
		if((new StringBuilder(str).reverse().toString()).equalsIgnoreCase(str)) {
			System.out.println("String is  Palindrome");
		}else {
			System.out.println("String is not Palindrome");
		}

	}

}
