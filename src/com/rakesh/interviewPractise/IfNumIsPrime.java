package com.rakesh.interviewPractise;

public class IfNumIsPrime {

	public static void main(String[] args) {
		int num = 15;
		
		boolean isPrime = false;
		
		for(int i = 2; i <= num/2; i++) {
			if(num%i == 0) {
				isPrime = true;
				break;
			}
		}
		if(!isPrime) {
			System.out.println("Number is Prime");
		}else {
			System.out.println("Number is not Prime");
		}
	}

}
