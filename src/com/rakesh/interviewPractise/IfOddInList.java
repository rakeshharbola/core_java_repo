package com.rakesh.interviewPractise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IfOddInList {
	static boolean ifOdd;
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(5);
		list.add(7);				
		/*list.stream()
			.forEach(e -> { 
				if(e%2==0) {
					ifOdd = true;				
				}
			});*/
		
		System.out.println("Is List only for Odd : "+list.stream()
		.anyMatch(e -> e%2 !=0));
		
		/*if(ifOdd) {
			System.out.println("List is not for odd only.");
		}else {
			System.out.println("List is only for odd.");
		}*/
		
	}

}
