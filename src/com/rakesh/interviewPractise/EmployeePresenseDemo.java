package com.rakesh.interviewPractise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Employee{
	private int empId;
	private String empName;
	public Employee(int empId, String empName) {
		super();
		this.empId = empId;
		this.empName = empName;
	}
	public int getEmpId() {
		return empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
		
}
public class EmployeePresenseDemo {

	public static void main(String[] args) {
		
		String allEmployees = "First,Second,Third,Forth"; 
		Employee emp1 = new Employee(1, "First");
		Employee emp2 = new Employee(2, "Second");
		Employee emp3 = new Employee(3, "Third");
		Employee emp4 = new Employee(4, "Forth");
		List<Employee> todayAttendence =new ArrayList<>();
		
		todayAttendence.add(emp1);
		todayAttendence.add(emp2);
		todayAttendence.add(emp3);
		todayAttendence.add(emp4);
		
		boolean result = isAllPresent(todayAttendence, allEmployees);
		System.out.println("All Present ? : "+result);

	}
	
	public static boolean isAllPresent(List<Employee> todayAttendence, String allEmployees) {
		return todayAttendence.stream().map(e -> e.getEmpName()).collect(Collectors.toList())
				              .containsAll(Arrays.asList(allEmployees.split(","))
											.stream()
											.collect(Collectors.toList()));
	}

}
