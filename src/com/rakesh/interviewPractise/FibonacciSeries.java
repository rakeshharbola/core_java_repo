package com.rakesh.interviewPractise;

import java.util.Scanner;

public class FibonacciSeries {

	public static void main(String[] args) {		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number till you want fibonacci series : ");
		int range = sc.nextInt();
		sc.close();
		int []result = fibonacciSeries(range); 
		
		for(int i=0; i<range; i++)
			System.out.print(result[i]+", ");
	}
	
	public static int[] fibonacciSeries(int range) {
		int []fbArr = new int[range];
		for(int i=0; i<range; i++) {
			if(i<=1) {
				fbArr[i] = i;
				}else {
					fbArr[i] = fbArr[i-1]+fbArr[i-2];
				}	
		}	
		return fbArr;
	}


}
// 1,1,2,3,5,8,13