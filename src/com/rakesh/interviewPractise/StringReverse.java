package com.rakesh.interviewPractise;

public class StringReverse {

	public static void main(String[] args) {
		String str = "Java is oop";
		
		String reverseStr = new StringBuilder(str).reverse().toString();
		
		System.out.println("String is : "+reverseStr);
		
		String []strArr = str.split(" ");
		for(int i=strArr.length-1; i>=0; i--) {
			System.out.print(strArr[i]+" ");
		}

	}

}
