package com.rakesh.stream;

public class Person {
	
	private Integer id;
	private String name;
	private Integer salary;
	
	public Person(Integer id, String name, Integer salary) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
	public Integer getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getSalary() {
		return salary;
	}
	public void setSalary(Integer salary) {
		this.salary = salary;
	}
	
	public String toString() {
		//return this.getId()+" = "+this.getName();
		return this.getName();
	}

}
