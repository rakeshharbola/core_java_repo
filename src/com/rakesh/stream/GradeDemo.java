package com.rakesh.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GradeDemo {
	
	public int countFloor(float a, float b) {
		return (int)Math.floor(a+b);
	}
	
	public String findWinner(String inputFirst, String inputSecond) {
		String []inputs = {inputFirst, inputSecond};
		int totalErica = 0;
		int totalBob = 0;
		for(String input : inputs) {
		for(int i=0; i<input.length(); i++) {
			if('E'==input.toCharArray()[i]) {
				totalErica = totalErica+1;}
			else if('M'==input.toCharArray()[i]) {
				totalErica = totalErica+3;}
			else {
				totalErica = totalErica+5;}
		}
		for(int i=0; i<inputSecond.length(); i++) {
			if('E'==inputSecond.toCharArray()[i]) {
				totalBob = totalBob+1;}
			else if('M'==inputSecond.toCharArray()[i]) {
				totalBob = totalBob+3;}
			else {
				totalBob = totalBob+5;}
		}
		}
		System.out.println(totalErica);
		System.out.println(totalBob);
		return totalErica>totalBob ? "Erica" :  totalErica<totalBob ? "Bob" : "Tie";
	}
	
	
	public static void main(String[] args) {
		List<Integer> grades = Arrays.asList(33,76,20,23,95,7,60,29,70,16,88,93,63,81,29,63,10,88,46,81,22,18,42,90,89,54,32,81,12,90,35,32,91,95);
		List<Integer> result = grades.stream()
			//.filter(i -> i>30)
			.map(i -> {
				if((5-i%5)<3 && i>=38) 
					i =i+(5-i%5);
				return i;
			}).collect(Collectors.toList());
		System.out.println(result);
		
		/*float a = 4.5f;
		float b = 1.6f;
		
		int result = new GradeDemo().countFloor(a, b);
		System.out.println(result);*/
		
	/*	String result = new GradeDemo().findWinner("EHH", "EME");
		System.out.println(result);*/
	}

}
