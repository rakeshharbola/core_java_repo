package com.rakesh.stream.newpractise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

class Animal{
	private int id;
	private String name;
	
	public Animal(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return this.name;
	}
} 

public class ListToMapConversion {

	public static void main(String[] args) {
		Animal dog = new Animal(1, "Dog");
		Animal cow = new Animal(2, "Cow");

		List<Animal> list = new ArrayList<>();
		list.add(dog);
		list.add(cow);
		
		Map<Integer, Animal> anmMap = converToMap(list);
		Map<Integer, Animal> anmMap2 = convertToMapWithJava8(list);
		System.out.println(anmMap);
		System.out.println(anmMap2);
		
		List<Integer> listInt = new ArrayList<>();
		listInt.add(1);
		listInt.add(2);
		listInt.add(3);
		listInt.add(4);
		listInt.add(2);
		listInt.add(3);
		listInt.add(1);
		listInt.add(2);
		
		// Set type is not consistent here (HashSet, LinkedHashSet)
		Set<Integer> set = listInt.stream().collect(Collectors.toSet());
		
		// List type is not consistent here (ArrayList, LinkedList)
		List<Integer> uniqueList = set.stream().collect(Collectors.toList());
		
		System.out.println("Elements in set is : "+set);
		System.out.println("Elements in unique List is : "+uniqueList);
		
		// To convert a stream into a specific type of List or Set
		
		List <Integer> arrayList = listInt.stream().collect(Collectors.toCollection(ArrayList::new));
		System.out.println("Elements in arrayList is : "+arrayList);
	
	}
	
	public static Map<Integer, Animal> converToMap(List<Animal> animal) {
		Map<Integer, Animal> map = new HashMap<>();
		for(Animal anm : animal) {
			map.put(anm.getId(), anm);
		}
		return map;
	}
	
	public static Map<Integer, Animal> convertToMapWithJava8(List<Animal> animals){
		return animals.stream().collect(Collectors.toMap(Animal::getId, Function.identity()));
		
	}

}
