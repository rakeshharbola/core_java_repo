package com.rakesh.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ReduceOperationDemo {

	public static void main(String[] args) {
		String str1 = "Java";
		String str2 = "is";
		String str3 = "Oops";
		
		String []arr = {str1, str2, str3};
		List<String> list = Arrays.asList(arr);
		String joinedString1 = list.stream().collect(Collectors.joining(",", "[", "]"));
		System.out.println("Joined String1 is : "+joinedString1);
		
		String joinedString2 = list.stream().collect(Collectors.joining());
		System.out.println("Joined String2 is : "+joinedString2);
		
		String joinedString3 = list.stream().collect(Collectors.joining("|"));
		System.out.println("Joined String3 is : "+joinedString3);
		

	}

}
