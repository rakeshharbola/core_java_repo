package com.rakesh.stream;

import java.util.stream.IntStream;

public class StreamForArray {

	public static void main(String[] args) {
		int []arr = {2,3,1,4,6,5};
		IntStream stream =  IntStream.of(arr);
		System.out.println(stream.sum());
		stream.skip(3).forEach(System.out::println);
	}

}
