package com.rakesh.stream;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MapWithFunctionDemo {

	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(3,1,4,9,2,5,7,6);
		
		Predicate<Integer> pred = new Predicate<Integer>() {
			public boolean test(Integer i) {
				return i%3==0;
			}
		};
		
		Function<Integer, Integer> fun = new Function<Integer, Integer>() {
			public Integer apply(Integer i) {
				return i*2;
			}
		};
		
		list.stream().filter(pred).map(fun).collect(Collectors.toList()).forEach(System.out::println);
	}

}
