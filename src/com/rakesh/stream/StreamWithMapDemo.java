package com.rakesh.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class StreamWithMapDemo {

	public static void main(String[] args) {
		List<Person> list = Arrays.asList(
				new Person(new Integer(1), "A", new Integer(10000)), 
				new Person(new Integer(2), "B", new Integer(20000)), 
				new Person(new Integer(3), "C", new Integer(30000)), 
				new Person(new Integer(4), "D", new Integer(40000)),
				new Person(new Integer(1), "B", new Integer(10000)), 
				new Person(new Integer(3), "F", new Integer(10000)));
		
		Map<Integer, List<Person>> map = list.stream()
										.collect(Collectors.groupingBy(Person::getId));//.forEach(System.out::println);
		
		Map<Integer, List<Person>> mapForSalary = list.stream().collect(Collectors.groupingBy(Person::getSalary));
		System.out.println(mapForSalary);
		
		Set<Map.Entry<Integer, List<Person>>> entry = map.entrySet();
		//entry.stream().forEach(System.out::println);
		
		Set<Integer> keySet = map.keySet();
		
	/*	for(Integer i : keySet) {
			System.out.println(i+" = "+map.get(i));
		}*/
		
		
		//map.values().forEach(System.out::println);
		
		map.values()
		   .stream()
		   .filter(i -> i.size()>1)
		   .forEach(person -> System.out.println("Thsese persons are having duplicate entry : "+person));
	}

}
