package com.rakesh.stream;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FilterDemo {

	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(2,5,3,1,4,6);
		
		Predicate pred = new Predicate<Integer>() {
			public boolean test(Integer i) {
				return i%2==0;
			}
		};
		
		System.out.println(list.stream().filter(pred).collect(Collectors.toList()));
	
	}
}
