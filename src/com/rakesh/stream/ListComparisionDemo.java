package com.rakesh.stream;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ListComparisionDemo {
		
	public static void main(String[] args) {		
		
		final String WSC_ONEMART_CONSTANT = "Wsc Onemart"; 
				
		List<String> first = new ArrayList<>(); 
		first.add("201901");
		first.add("201903");
		first.add("201905");
		first.add("201908");
		first.add("202001");
		
		Map<String, List<String>> businessList  = new LinkedHashMap<>();
		businessList.put("Wsc Onemart", first);
		
		List<String> secondList = new ArrayList<>(); 
		secondList.add("201903");
		secondList.add("201905");
		secondList.add("202001");
		
		businessList.get(WSC_ONEMART_CONSTANT)
			.removeIf(reportingPeriod -> {
				return ListComparisionDemo.compareReportingPeriod(reportingPeriod, secondList) == 1;
		});		
		
		System.out.println(businessList);
	}
	
	public static byte compareReportingPeriod(String reportingPeriod, List<String> secondList) {
		if(!secondList.contains(reportingPeriod)) {
			return 1;
		}else {
			return -1;
			}
		}
}
