package com.rakesh.stream;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;

public class ReduceWithBinaryOperationDemo {

	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9);
		BinaryOperator<Integer> bin = new BinaryOperator<Integer>() {
			public Integer apply(Integer a, Integer b) {
				return a+b;
			}
		};
		
		Predicate<Integer> pred = new Predicate<Integer>() {
			public boolean test(Integer i) {
				return i%3==0;
			}
		};		
		
		Function<Integer, Integer> fun = new Function<Integer, Integer>() {
			public Integer apply(Integer i) {
				return i*i;
			}
		};
		
		System.out.println(list.stream()
			.filter(pred)
			.map(fun)
			.reduce(bin)
			.get());
	}

}
