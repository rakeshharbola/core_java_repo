package com.rakesh.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ListToMap {
	
	public Function<Person, String> getMapKeys(){
		Function<Person, String> fun = e -> {
			if(e.getSalary()>0 && e.getSalary() < 10000)
				return "0_10000";
			if(e.getSalary()>10001 && e.getSalary() < 20000)
				return "10000_20000";
			if(e.getSalary()>20001 && e.getSalary() < 30000)
				return "20000_30000";
			if(e.getSalary()>30001 && e.getSalary() < 40000)
				return "30000_40000";
			return "Out of Range";
		};
		return fun;
	}
	
	public Map<String, List<Person>> getPersonBySalary(){
		
		Map<String, List<Person>> map = getPersonList().stream()
				.collect(Collectors.groupingBy(getMapKeys()));
		
		return map;
	}
	
	public static List<Person> getPersonList(){
		return Arrays.asList(
				new Person(new Integer(1), "A", new Integer(8000)), 
				new Person(new Integer(2), "B", new Integer(12000)), 
				new Person(new Integer(3), "C", new Integer(16000)), 
				new Person(new Integer(4), "D", new Integer(22000)),
				new Person(new Integer(1), "B", new Integer(36000)), 
				new Person(new Integer(3), "D", new Integer(26000)));
	}
	
	public static void main(String[] args) {
		ListToMap obj = new ListToMap();
		Map<String, List<Person>> map = obj.getPersonBySalary();		
		map.keySet().stream().forEach(e -> {
			System.out.println(e+" = "+map.get(e));
		});
		List<Person> list = new ArrayList<Person>();;
		int i=0;
		while(i<10000) {
			try {
				list.addAll(getPersonList());
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

}
