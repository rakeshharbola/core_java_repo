package com.rakesh.json;

import java.io.FileNotFoundException;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

public class Testmain {
	
	public static void main(String[] args) throws FileNotFoundException {
		ItemResultSet rs = new ItemResultSet();		
		ItemInfo []itemInfoArr=null;
		String []cNames=null;
		JSONObject json = new JSONObject("{\r\n" + 
				"  \"results\": \r\n" + 
				"		[\r\n" + 
				"			{\"objectId\":\"abcdef\", \"objectType\":\"marketing_contract\", \"mimeType\":\"application/pdf\", \"properties\":{\"acct_id\":\"111111,222222\","
				+ " \"acct_name\":\"firstOne\"}},\r\n" + 
				"			{\"objectId\":\"ghijkl\", \"objectType\":\"marketing_contract\", \"mimeType\":\"application/pdf\", \"properties\":{\"acct_id\":\"555555,666666,777777\","
				+ " \"acct_name\":\"SecOne\"}},\r\n" + 
				"			{\"objectId\": \"mnopqr\", \"objectType\":\"marketing_contract\", \"mimeType\":\"application/pdf\", \"properties\":{\"acct_id\":\"888888,999999\","
				+ " \"acct_name\":\"ThOne\"}}\r\n" + 
				"		]\r\n" + 
				"}");		
		JSONArray array = json.getJSONArray("results");	
		int infoArrayIndex=0;
		itemInfoArr = new ItemInfo[array.length()];
		for(int m=0; m<array.length(); m++) {
			JSONObject jsonObject = array.getJSONObject(m);
			JSONObject propertiesArray = jsonObject.getJSONObject("properties");      
            
               
       //     for(int s=0; s<propertiesArray.length(); s++) {
          //  	JSONObject propJsonObject = propertiesArray.get
            Set<String> columns = propertiesArray.keySet();
            	cNames = new String[propertiesArray.keySet().size()];
            	String [][]cValues = new String[2][3];
            	int colNameIndex=0;
            	for(Object propKey : columns){            
                    cNames[colNameIndex]=propKey.toString();
                    String val = propertiesArray.get(propKey.toString()).toString();
                    String []tempValArr;
                    tempValArr = val.split(",");                    
                  	for(int w=0; w<tempValArr.length; w++) {
                    		cValues[colNameIndex][w]=tempValArr[w];                     		
                    }      
                  	colNameIndex++;                  
                }
            	ItemInfo itemInfo = new ItemInfo();
           // 	if(propJsonObject.keySet().size()==cValues.length) {
            		
        			itemInfo.setColumnValues(cValues);
                    itemInfo.setItemURN(jsonObject.getString("objectId"));
                    itemInfo.setMimeType(jsonObject.getString("mimeType"));
        	//	}
            	if(itemInfoArr[itemInfoArr.length-1]==null){
            		itemInfoArr[infoArrayIndex]=itemInfo;
            		infoArrayIndex++;
            	}
            /*	if(cNames.length == propJsonObject.keySet().size()) {            		
            		rs.setColumnNames(cNames);            		            		
            	} if(itemInfoArr[itemInfoArr.length-1]==null) {
            		rs.setItemInfos(itemInfoArr);
            	}*/
            	
           //  }             
        }
		rs.setColumnNames(cNames);
		rs.setItemInfos(itemInfoArr);
		Main.printInfo(rs);
		System.out.println("Done");
	}
	
	public static void printInfo(ItemResultSet rs) {
     	
            	System.out.println("Columns name : ");		
        		for(int z=0; z<rs.getColumnNames().length; z++) {
        			System.out.println(rs.getColumnNames()[z]);
        		}
        		        	
        		/*for(int y=0; y<rs.getItemInfos().length; y++) {
        			System.out.println(rs.getItemInfos()[y].getItemURN());
        			System.out.println(rs.getItemInfos()[y].getMimeType());
        			for(int x=0; x<rs.getItemInfos().length; x++) {
        				for(int z=0; z<rs.getItemInfos()[y].getColumnValues()[x].length; z++) {
        				System.out.println(rs.getItemInfos()[x].getColumnValues()[x][z]);
        				}
        			}
        		}*/
        		System.out.println("Row 1 Column Values");
        		
        		System.out.println(rs.getItemInfos()[0].getColumnValues()[0][0]);
        		System.out.println(rs.getItemInfos()[0].getColumnValues()[0][1]);
        		System.out.println(rs.getItemInfos()[0].getColumnValues()[1][0]);
        		
        		System.out.println("Row 2 Column Values");
        		System.out.println(rs.getItemInfos()[1].getColumnValues()[0][0]);
        		System.out.println(rs.getItemInfos()[1].getColumnValues()[0][1]);
        		System.out.println(rs.getItemInfos()[1].getColumnValues()[0][2]);
        		System.out.println(rs.getItemInfos()[1].getColumnValues()[1][0]);
        		
        		System.out.println("Row 3 Column Values");
        		System.out.println(rs.getItemInfos()[2].getColumnValues()[0][0]);
        		System.out.println(rs.getItemInfos()[2].getColumnValues()[0][1]);
        		System.out.println(rs.getItemInfos()[2].getColumnValues()[1][0]);
      
	}
	
	/*ItemResultSet setPropertiesNames(JSONObject jsonObject) {
    	JSONArray propertiesArray = jsonObject.getJSONArray("properties");      
        int i=0;
        Iterator<Object> propIterator = propertiesArray.iterator();
        while(propIterator.hasNext()) {
        	JSONObject propJsonObject = (JSONObject) propIterator.next();
        	cNames = new String[propJsonObject.keySet().size()];
        	for(String propKey : propJsonObject.keySet()){            
                cNames[i]=propKey.toString();
                
                i++;
            }
        	if(cNames.length == propJsonObject.keySet().size()) {
        		rs.setColumnNames(cNames);
        	}            	            
        }  
        return rs;
    }*/
	
	

}
