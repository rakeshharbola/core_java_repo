package com.rakesh.json;

public class ItemInfo {

	private String [][]columnValues;
	private String itemURN;
	private String mimeType;
	
	public String[][] getColumnValues() {
		return columnValues;
	}
	public void setColumnValues(String[][] columnValues) {
		this.columnValues = columnValues;
	}	
	public String getItemURN() {
		return itemURN;
	}
	public void setItemURN(String itemURN) {
		this.itemURN = itemURN;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	
	
}
