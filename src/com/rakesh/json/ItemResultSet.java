package com.rakesh.json;

public class ItemResultSet {

	String []columnNames;
	ItemInfo []itemInfos;
	
	public String[] getColumnNames() {
		return columnNames;
	}
	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}
	public ItemInfo[] getItemInfos() {
		return itemInfos;
	}
	public void setItemInfos(ItemInfo[] itemInfos) {
		this.itemInfos = itemInfos;
	}
		
}
