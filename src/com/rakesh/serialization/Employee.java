package com.rakesh.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Employee implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private transient int id;
	
	public Employee(String name, int id) {
		super();
		this.name = name;
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	
	private void writeObject(ObjectOutputStream op) throws IOException {
		op.defaultWriteObject();
		int idNew = 1899;
		op.writeObject(idNew);		
	}
	
	private void readObject(ObjectInputStream op) throws IOException, ClassNotFoundException {
		op.defaultReadObject();
		int idNew = (Integer)op.readObject();
		id = idNew;
	}


	public static void main(String[] args) throws Exception{
		
		Employee emp =new Employee("Rakesh", 1890);
		
		FileOutputStream fo = new FileOutputStream("D:\\emp.ser");
		ObjectOutputStream out = new ObjectOutputStream(fo);
		out.writeObject(emp);		
		out.close();
		
		System.out.println("Employee Before Deserialization : "+emp.getName()+" "+emp.getId() );
		
		FileInputStream fi = new FileInputStream("D:\\emp.ser");
		ObjectInputStream in = new ObjectInputStream(fi);
		Employee empNew = (Employee)in.readObject();
		in.close();
		
		System.out.println("Employee after Deserialization : "+empNew.getName()+" "+empNew.getId() );
		
	}

}
