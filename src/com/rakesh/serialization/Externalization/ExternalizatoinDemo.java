package com.rakesh.serialization.Externalization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ExternalizatoinDemo {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Person person = new Person();
		person.setName("Rakesh");
		person.setAge(25);
		person.setAddress("Faridabad");
		
		FileOutputStream fout = new FileOutputStream("D:\\person.ser");
			ObjectOutputStream out = new ObjectOutputStream(fout);
			out.writeObject(person);
			out.close();
			
		FileInputStream fin = new FileInputStream("D:\\person.ser");
		ObjectInputStream in = new ObjectInputStream(fin);
		Person personNew  = (Person)in.readObject();
		
		System.out.println("Person name : "+personNew.getName()+ " of age "+personNew.getAge()+" belongs to "+personNew.getAddress());
				
	

	}

}
