package com.rakesh.multithreading;

import java.util.Random;

public class LockConditionProducerConsumerDemo {

	public static void main(String[] args) {
		MyBlockingQueue blockingQueue = new MyBlockingQueue(5);
		
		final Runnable producer = () -> {
				System.out.println("Inside producer : "+ " : "+Thread.currentThread().getName()+" Size : "+blockingQueue.getQueue().size());
				try {
					blockingQueue.put(new Random().nextInt());
				}catch(Exception e) {
					e.printStackTrace();
				}
		};
		
		final Runnable consumer = () -> {
				try {
					System.out.println("Inside consumer : "+ " : "+Thread.currentThread().getName()+" Size : "+blockingQueue.getQueue().size());
					Integer item = blockingQueue.take();
					System.out.println("Item : "+item);
				}catch(Exception e) {
					e.printStackTrace();
				}
		};
		
		new Thread(producer).start();
		new Thread(producer).start();
		new Thread(consumer).start();
		new Thread(consumer).start();
	}
 
}
