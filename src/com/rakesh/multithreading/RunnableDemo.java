package com.rakesh.multithreading;

public class RunnableDemo implements Runnable{
	
	public void run(){
		System.out.println("I am inside runnable run method");
	}
	
	public static void main(String []args) {
		int coreCount = Runtime.getRuntime().availableProcessors();
		System.out.println("Total Available Processors is : "+coreCount);
		RunnableDemo run1 = new RunnableDemo();
		Thread t1 = new Thread(run1);
		t1.start();
	}

}
