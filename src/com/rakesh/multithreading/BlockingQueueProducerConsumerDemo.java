package com.rakesh.multithreading;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockingQueueProducerConsumerDemo {
		
	public static void main(String[] args) {
		BlockingQueue<Integer> queue = new LinkedBlockingQueue<Integer>(10);
		
		final Runnable producer = () ->  {
			System.out.println("I am producer : "+Thread.currentThread().getName());
		//	while(true) {
				try {
					System.out.println("Before in Producer : "+queue+" ... "+Thread.currentThread().getName());
					queue.put (new Random().nextInt());
					System.out.println("After in Producer : "+" ... "+queue+Thread.currentThread().getName());
				}catch(InterruptedException e) {
					e.printStackTrace();
				}
		//	}
		}; 
		
				
		final Runnable consumer = () -> {
			System.out.println("I am Consumer : "+Thread.currentThread().getName());
		//	while(true) {
				try {
					System.out.println("Before in Consumer : "+queue+" ... "+Thread.currentThread().getName());
					queue.take();
					System.out.println("After in Consumer : "+queue+" ... "+Thread.currentThread().getName());
				}catch(InterruptedException e) {
					e.printStackTrace();
				}
		//	}	
		};
		
		
		new Thread(producer).start();
		new Thread(producer).start();
		new Thread(consumer).start();
		new Thread(consumer).start();

	}

}


