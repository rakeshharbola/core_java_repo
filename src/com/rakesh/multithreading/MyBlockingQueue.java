package com.rakesh.multithreading;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MyBlockingQueue {
	private Queue<Integer> queue;
	private int max = 16;
	public ReentrantLock lock = new ReentrantLock(true);
	private Condition notFull = lock.newCondition();
	private Condition notEmpty = lock.newCondition();
	
	public MyBlockingQueue(int size) {
		queue = new LinkedList<Integer>();
		this.max = size;
	}
	
	public void put(Integer i) throws InterruptedException{
		lock.lock();
		System.out.println("Lock held by Put : "+Thread.currentThread().getName());
		try {
			if(queue.size() == max) {
				notFull.await();
			}
			queue.add(i);
			notEmpty.signalAll();
		}finally {			
			lock.unlock();
			System.out.println("Put lock released.."+Thread.currentThread().getName());
		}
		
	}
	
	public Integer take() throws InterruptedException{
		lock.lock();
		System.out.println("Lock held by Take : "+Thread.currentThread().getName());
		try {
			while(queue.size() == 0) {
				notEmpty.await();
			}
			Integer i = queue.remove();
			notFull.signalAll();		
			return i;
		}finally {
			System.out.println("Take lock released.."+Thread.currentThread().getName());
			lock.unlock();
		}
	}

	@Override
	public String toString() {
		return "MyBlockingQueue [queue=" + queue + "]";
	}

	public Queue<Integer> getQueue() {
		return queue;
	}

}
