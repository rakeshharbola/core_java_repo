package com.rakesh.Math;

import java.util.Scanner;

public class SquareRoot {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number to find it's square root : ");
		
		double n = sc.nextDouble();
		
		double root = Math.sqrt(n);
		
		System.out.println("The square root of "+ n + " is : "+root);
		
		sc.close();

	}

}
