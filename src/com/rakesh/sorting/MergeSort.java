package com.rakesh.sorting;

public class MergeSort {
	public void mergeSort(int []arr, int start, int end) {
		if(start >= end) {
			return;
		}
		int mid = (start+end)/2;
		System.out.println("Start = "+start+" Mid = "+mid+" End = "+end);
		mergeSort(arr, start, mid);
		mergeSort(arr, mid+1,  end);
		merge(arr, start, mid, end);
	}
	
	public void merge(int []arr, int start, int mid, int end) {
		int []merged = new int[arr.length];
		
		int idx1 = start;
		int idx2 = mid+1;
		int x=0;
		
		while(idx1 <= mid && idx2 <= end ) {
			if(arr[idx1] <= arr[idx2]) {
				merged[x] = arr[idx1];
				x++;
				idx1++;
			}else {
				merged[x] = arr[idx2];
				x++;
				idx2++;
			}
		}
		
		while(arr[idx1] <= arr[mid]) {
			merged[x++] = arr[idx1++];
		}

		while(arr[idx2] <= arr[end]) {
			merged[x++] = arr[idx2++];
		}

		/*for(int i=0, j=start; i<merged.length; i++, j++) {
			arr[j] = merged[i];
		}*/
			
		for(int i=0; i<merged.length; i++) {
			System.out.print(merged[i]);
		}
		System.out.println();
	}
	public static void main(String[] args) {
		
		int []arr = {6,5,4,3,2,1};
		int start = 0;
		int end = arr.length;			
		new MergeSort().mergeSort(arr, start, end-1);
	}
}
