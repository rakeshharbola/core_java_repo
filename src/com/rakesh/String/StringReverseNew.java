package com.rakesh.String;

public class StringReverseNew {

	public static void main(String[] args) {
		String name="Rakesh";
		String reverse = new StringBuffer(name).reverse().toString();
		System.out.println("Actual Name is  : "+name);
		System.out.println("Reversed Name is : "+reverse);
		
		String reverse1 = StringReverseNew.reverse("Sanjay");
		System.out.println("New name is : "+reverse1);

	}
	
	public static String reverse(String name) {
		String newName = "";				
		if(name == null || name.isEmpty())
			return newName;
		for(int i = name.length()-1; i >=0; i--) {
			newName = newName+name.charAt(i);
		}
		return newName;
	}
	

}
