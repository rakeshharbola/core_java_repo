package com.rakesh.String;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class User{
	private String name;
	private String city;
	private String street;
	
	public User(String name, String city, String street) {
		super();
		this.name = name;
		this.city = city;
		this.street = street;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	
}

public class StringReplaceDemo {
	
	public User decodeValues(Map<String, String> scMap, User user) {
		Set<String> encodes = scMap.keySet();
		for(String key : encodes) {
			if(user.getName().contains("encode")) {
				if(user.getName().contains(key)) {
					user.setName(user.getName().replace(key, scMap.get(key)));
				}
			}
			
			if(user.getCity().contains("encode")) {
				if(user.getCity().contains(key)) {
					user.setCity(user.getCity().replace(key, scMap.get(key)));
				}
			}
			
			if(user.getStreet().contains("encode")) {
				if(user.getStreet().contains(key)) {
					user.setStreet(user.getStreet().replace(key, scMap.get(key)));
				}
			}
		}
		
		return user;
	}
	
	public static void main(String[] args) {
		Map<String, String> scMap = new HashMap<>();
		scMap.put("encode123encode", "{");
		scMap.put("encode124encode", "|");
		scMap.put("encode125encode", "}");
		scMap.put("encode126encode", "~");
		scMap.put("encode33encode", "!");
		scMap.put("encode34encode", "\"");
		scMap.put("encode35encode", "#");
		scMap.put("encode36encode", "$");
		scMap.put("encode37encode", "%");
		scMap.put("encode38encode", "&");		
		scMap.put("encode39encode", "\'");
		scMap.put("encode40encode", "(");
		scMap.put("encode41encode", ")");
		scMap.put("encode42encode", "*");
		scMap.put("encode43encode", "+");
		scMap.put("encode44encode", ",");
		scMap.put("encode45encode", "-");
		scMap.put("encode46encode", ".");
		scMap.put("encode95encode", "_");
		
		User user = new User("encode123encodeRakeshencode123encodeHarbolaencode95encode", "encode125encodeCityencode46encodeValueencode95encode", "encode41encodeStreetencode43encode");
		user = new StringReplaceDemo().decodeValues(scMap, user);
		
		System.out.println(user.getName());
		System.out.println(user.getCity());
		System.out.println(user.getStreet());
	}

}

