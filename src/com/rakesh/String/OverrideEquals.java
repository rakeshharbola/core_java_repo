package com.rakesh.String;

public class OverrideEquals {
	
	private double re;
	private double cm;
	
	public OverrideEquals(double re, double cm) {
		this.re = re;
		this.cm = cm;
	}
	
	@Override
	public boolean equals(Object o) {
		
		if(o == this)
			return true;
		
		if(!(o instanceof OverrideEquals))
			return false;
		
		OverrideEquals oe = (OverrideEquals)o;
		
		return Double.compare(re, oe.re) == 0 &&
				Double.compare(cm, oe.cm) == 0;
		
	}

	public static void main(String[] args) {
		OverrideEquals oe1 = new OverrideEquals(3.5, 4.5);
		OverrideEquals oe2 = new OverrideEquals(3.6, 4.5);
		OverrideEquals oe3 = new OverrideEquals(3.6, 4.5);
		
		System.out.println(oe1.equals(oe2));
		System.out.println(oe3.equals(oe2));

	}

}
