package com.rakesh.String;

public class StringReverse {
	
	public static String reverse(String name) {
		String newName = "";
		if(name.isEmpty() || name==null)
			return name;
		for(int i = name.length()-1; i>=0; i--) {
			newName = newName+name.charAt(i);
		}
		return newName;
	}
	public static String reverseName(String name) {
		StringBuilder reverseName = new StringBuilder();
		char[] arr = name.toCharArray();
		for(int i=arr.length-1; i>=0; i--) {
			reverseName = reverseName.append(arr[i]);
		}
		return reverseName.toString();
	}
	public static void main(String[] args) {
		
		System.out.println(reverseName("Rakesh"));
		System.out.println(reverse("Great India"));
	}

}
