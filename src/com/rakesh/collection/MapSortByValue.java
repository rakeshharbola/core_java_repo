package com.rakesh.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MapSortByValue {

	public static void main(String[] args) {
		Map<Integer, String> map = new HashMap<Integer, String>();

		map.put(2, "XYZ");
		map.put(5, "JKL");
		map.put(4, "GHI");
		map.put(3, "DEF");
		map.put(1, "ABC");
		

		/*Map<Integer, String> sortedMap = map.entrySet().stream()
				//.peek(System.out::println)
				.sorted((e1, e2) -> {
						return e1.getValue().compareTo(e2.getValue());
					})
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (e1,e2) -> e2, LinkedHashMap::new));
		
		Map<Integer, String> sortedMap2 = map.entrySet().stream()
				//.peek(System.out::println)
				.sorted(Map.Entry.comparingByValue())
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (e1,e2) -> e2, LinkedHashMap::new));
		
		Map<Integer, String> sortedMap3= map.entrySet().stream()
				.peek(System.out::println)
				.sorted(Map.Entry.comparingByValue())
				.collect(Collectors.toMap(Map::getKey, Map.Entry::getKey, (e1,e2) -> e2, LinkedHashMap::new));
		
		
		LinkedHashMap<Integer, String> sortedMap4 = new LinkedHashMap<Integer, String>(); 
		map.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByValue())
				.forEachOrdered(x -> sortedMap4.put(x.getKey(), x.getValue()));
		
		
		
		List<Map.Entry<Integer, String>> arrayList = new ArrayList<Map.Entry<Integer, String>>(map.entrySet());
		
		LinkedHashMap<Integer, String> newSortedHashMap = arrayList.stream()
		.sorted(Map.Entry.comparingByValue())
		//.peek(System.out::println)
		.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2, LinkedHashMap::new));
		
		
		//System.out.println(sortedMap);
		
		//System.out.println(sortedMap2);
				
		//	System.out.println(sortedMap4);
		
		System.out.println(newSortedHashMap);*/
		List<Map.Entry<Integer, String>> arrayList = new ArrayList<Map.Entry<Integer, String>>(map.entrySet());
		
		Collections.sort(arrayList, (o1, o2) -> {
			return (o1.getValue()).compareTo(o2.getValue());
		});
		
		Map<Integer, String> newHashMap = new LinkedHashMap<>();
		
		for(Map.Entry<Integer, String> entry : arrayList) {
			newHashMap.put(entry.getKey(), entry.getValue());
		}
		
		System.out.println(newHashMap);
	}
	

}
