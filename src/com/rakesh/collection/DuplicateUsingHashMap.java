package com.rakesh.collection;

import java.util.HashMap;
import java.util.Map;

public class DuplicateUsingHashMap {

	public static void main(String[] args) {
		String str = "Hello world, Welcome Hello to Simplilearn";
		
		String []split = str.split(" ");
		
		Map<String, Integer> map = new HashMap<>();
		for(int i=0; i<split.length; i++) {
			
			if(map.containsKey(split[i])) {
				int count = map.get(split[i]);
				map.put(split[i], count+1);
			}else {
				map.put(split[i], 1);
			}
		}
		System.out.println(map);
	}

}
