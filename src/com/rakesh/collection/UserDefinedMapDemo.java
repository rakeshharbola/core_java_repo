package com.rakesh.collection;

import java.util.HashMap;
import java.util.Map;

public class UserDefinedMapDemo {

	public static void main(String[] args) {
		Employee emp = new Employee(1890, "Rakesh");
		Employee emp2 = new Employee(1890, "Rakesh");
		Map<Employee, String> map = new HashMap<Employee, String>();
		
		map.put(emp, "Alok");
	/*	List<String> listNew = new ArrayList<String>(map.values());
		System.out.println("List is : "+listNew);*/
		
		System.out.println(emp.hashCode());
		System.out.println(emp2.hashCode());
		map.put(emp2, "Soni");
		
		System.out.println(map.size());
		
		System.out.println(map.get(emp));
	}

}
