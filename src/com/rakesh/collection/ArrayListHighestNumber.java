package com.rakesh.collection;

import java.util.Arrays;
import java.util.List;

public class ArrayListHighestNumber {
	
	public static void main(String []args) {
		Integer []array = {1, 2, 3, 4, 11, 12, 13, 14, 21, 22, 23, 24, 31, 32};
	//	List<Integer> list = Arrays.asList(array);
		int high=0;
		int secondHigh=0;
		
		System.out.println("Length of array is : "+array.length);
		for(int i=0; i<array.length; i++) {
			if(array[i] > high) {
				secondHigh = high;
				high = array[i];
			}				
		}
		System.out.println("Second High Number is : "+secondHigh);
	}

}
