package com.rakesh.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListDemo {

	public static void main(String[] args) {
		List<Integer> arrayList = new ArrayList<Integer>();
		arrayList.add(1);
		arrayList.add(3);
		arrayList.add(5);
		arrayList.add(2);
		arrayList.add(4);
		
		arrayList.forEach(System.out::println);
		
		arrayList.stream().limit(3).forEach(System.out::println);
		
		Iterator<Integer> it = arrayList.iterator();
		while(it.hasNext()) {
			System.out.print(it.next());
		}
		
		arrayList.add(2, 7);
		
		arrayList.forEach(System.out::println);
		
		
		arrayList.clear();
		
		System.out.println("List values are : "+arrayList);
	}

}
