package com.rakesh.collection;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RemoveDuplicateAList {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("Rakesh");
		list.add("Sanjay");
		list.add("Harshil");
		list.add("Rakesh");
		System.out.println("List before removing duplicate elements : "+list);
		Set<String> set = new LinkedHashSet<>(list);
		list.clear();
		list.addAll(set);
		System.out.println("List after removing duplivate elements : "+list);
		//System.out.println(set);
		
		List<String> uniqueList = list.stream().distinct().collect(Collectors.toList());
		ArrayList<String> uniqueList2 = list.stream().distinct().collect(Collectors.toCollection(ArrayList::new));
		System.out.println(uniqueList);
		System.out.println(uniqueList2);
	}

}
