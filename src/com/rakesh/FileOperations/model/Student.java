package com.rakesh.FileOperations.model;

public class Student {
	private String name;
	private String age;
	private String clas;
	private String subject;
	private String marks;
	public Student(String name, String age, String clas, String subject, String marks) {
		super();
		this.name = name;
		this.age = age;
		this.clas = clas;
		this.subject = subject;
		this.marks = marks;
	}
	public String getName() {
		return name;
	}
	public String getAge() {
		return age;
	}
	public String getClas() {
		return clas;
	}
	public String getSubject() {
		return subject;
	}
	public String getMarks() {
		return marks;
	}
	
}
