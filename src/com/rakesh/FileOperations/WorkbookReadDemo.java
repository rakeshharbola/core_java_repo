package com.rakesh.FileOperations;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class WorkbookReadDemo {
	
	public static void main(String []args) throws IOException{
		try(FileInputStream fin = new FileInputStream("C:\\Users\\rakesh.harbola\\Desktop\\Files\\rakesh.xls")){
			Workbook wb = new HSSFWorkbook(fin);
			Sheet sheet = wb.getSheet("Student");
			/*Iterator<Row> it = sheet.iterator();
			while(it.hasNext()) {
				int cell = 0;
				Row row = it.next();
				System.out.println(row.getCell(cell)+"	"+row.getCell(++cell)+"	"+row.getCell(++cell)+" "+row.getCell(++cell)+"	"+row.getCell(++cell));
			}*/
			
			sheet.forEach(row -> { 
				int cell = 0;
				int marks = Integer.parseInt(row.getCell(row.getLastCellNum()).toString());
				if(row.getCell(0).equals("Rakesh") && row.getCell(++cell).equals("28"))
					marks = Integer.parseInt(row.getCell(row.getLastCellNum()).toString())*2; 
					System.out.println(row.getCell(cell)+"	"+row.getCell(++cell)+"	"+row.getCell(++cell)+" "+row.getCell(++cell)+"	"+marks);
			});
			wb.close();
		}
	}
}
