package com.rakesh.FileOperations;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BufferReaderFileReadDemo {

	public static void main(String[] args) throws IOException {
		FileReader file = new FileReader("C:\\Users\\rakesh.harbola\\Desktop\\Files\\test.txt");
		BufferedReader buffer = new BufferedReader(file);
		String st;
		buffer.lines().forEach(System.out::println);
		/*while((st = buffer.readLine()) != null) {
			System.out.println(st);
		}*/
		buffer.close();
		file.close();
	}

}
