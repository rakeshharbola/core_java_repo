package com.rakesh.FileOperations;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.rakesh.FileOperations.model.Student;

public class WorkbookReadAndWriteDemo {
	Workbook wb = new HSSFWorkbook();
	int row=0;
	final int finalRow=0;
	private Sheet sheet1 = wb.createSheet("Student");
		
	public void createSheet() {
		
		try(OutputStream os = new FileOutputStream("C:\\Users\\rakesh.harbola\\Desktop\\Files\\rakesh.xls")){
			String []headers = getHeaders();
			writeHeaders(headers);
			ArrayList<Student> studentList = getStudentList();
			writeSheet(studentList);
			wb.write(os);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public String[] getHeaders() {
		String []headers = {"Name", "Age", "Class", "Subject", "Marks"};
		return headers;
	}
	
	public void writeHeaders(String[] headers) {
		sheet1.createRow(0);
		for(int i=0; i<headers.length; i++) {
			sheet1.getRow(0).createCell(i).setCellValue(headers[i]);
		}
	}
	
	public void writeSheet(ArrayList<Student> studentList) {
		Iterator<Student> it = studentList.iterator();  
		while(it.hasNext()) {
			int newRow=++row, cell=0;
			sheet1.createRow(newRow);
			Student next = it.next();
			sheet1.getRow(newRow).createCell(cell).setCellValue(next.getName());
			sheet1.getRow(newRow).createCell(++cell).setCellValue(next.getAge());
			sheet1.getRow(newRow).createCell(++cell).setCellValue(next.getClas());
			sheet1.getRow(newRow).createCell(++cell).setCellValue(next.getSubject());
			sheet1.getRow(newRow).createCell(++cell).setCellValue(next.getMarks());
		}
	}
	
	public void writeSheetWithStream(ArrayList<Student> studentList) {
		studentList.stream().forEach(i -> {
			int newRow=1, cell=0;
			sheet1.createRow(newRow);
			sheet1.getRow(newRow).createCell(cell).setCellValue(i.getName());
			sheet1.getRow(newRow).createCell(++cell).setCellValue(i.getAge());
			sheet1.getRow(newRow).createCell(++cell).setCellValue(i.getClas());
			sheet1.getRow(newRow).createCell(++cell).setCellValue(i.getSubject());
			sheet1.getRow(newRow).createCell(++cell).setCellValue(i.getMarks());
		});
	}
	
	public ArrayList<Student> getStudentList(){
		ArrayList<Student> studentList = new ArrayList<Student>();
		studentList.add(new Student("Rakesh", "28", "8", "Hindi", "60"));
		studentList.add(new Student("Sanjay", "30", "10", "English", "70"));
		studentList.add(new Student("Ghanshyam", "34", "11", "Math", "80"));
		studentList.add(new Student("Deepak", "32", "12", "History", "90"));
		return studentList;
	}
	
	public static void main(String[] args) {
		WorkbookReadAndWriteDemo obj = new WorkbookReadAndWriteDemo();
		obj.createSheet();
	}

}
