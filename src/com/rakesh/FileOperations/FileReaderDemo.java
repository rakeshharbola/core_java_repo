package com.rakesh.FileOperations;

import java.io.FileReader;
import java.io.IOException;

public class FileReaderDemo {
	
	public static void main(String []args) throws IOException{
		FileReader fileReader = new FileReader("C:\\Users\\rakesh.harbola\\Desktop\\Files\\test.txt");
		int i;
		while((i = fileReader.read()) != -1) {
			System.out.print((char) i);
		}
		fileReader.close();
	}

}
