package com.rakesh.ModelMapperDemo;

import org.modelmapper.Condition;
import org.modelmapper.ModelMapper;

public class ModelMapperDemo {

	public static void main(String[] args) {
		ModelMapper modelMapper = new ModelMapper();
		EmployeeFirst emp1 = new EmployeeFirst(1, "Rakesh", "9015047475", "");
		EmployeeSecond emp2 = new EmployeeSecond(2, "Rakesh 2", "9015047456", "124");
		
		
		System.out.println("Emp 1 id :"+emp1.getId());
		System.out.println("Emp 1 Name :"+emp1.getName());
		System.out.println("Emp 1 First Ph No :"+emp1.getFirstPhNo());
		System.out.println("Emp 1 Second Ph No :"+emp1.getSecondPhNo());
		System.out.println("Before using Model Mapper");
		System.out.println("Emp 2 id :"+emp2.getId());
		System.out.println("Emp 2 Name :"+emp2.getName());
		System.out.println("Emp 2 First Ph No :"+emp2.getFirstPhNo());
		System.out.println("Emp 2 Second Ph No :"+emp2.getSecondPhNo());
		
		System.out.println("After using Model Mapper");
		
		/*Condition notEmpty =
			    ctx -> ctx.getSource() != "";*/
		
		if(!emp1.getSecondPhNo().isEmpty()) {
			System.out.println("Called");
			modelMapper.createTypeMap(EmployeeFirst.class, EmployeeSecond.class);
			modelMapper.getTypeMap(EmployeeFirst.class, EmployeeSecond.class).addMapping(EmployeeFirst::getSecondPhNo, EmployeeSecond::setSecondPhNo);
		}
	//	modelMapper.getTypeMap(EmployeeFirst.class, EmployeeSecond.class).addMapping(mapper -> mapper.when(notEmpty), EmployeeSecond::setSecondPhNo);
		
		System.out.println("Emp 2 Second Ph No :"+emp2.getSecondPhNo());
	}
	
	

}
