package com.rakesh.ModelMapperDemo;

public class EmployeeFirst {
	private int id;
	private String name;
	private String firstPhNo;
	private String secondPhNo;
	
	public EmployeeFirst(int id, String name, String firstPhNo, String secondPhNo) {
		super();
		this.id = id;
		this.name = name;
		this.firstPhNo = firstPhNo;
		this.secondPhNo = secondPhNo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstPhNo() {
		return firstPhNo;
	}
	public void setFirstPhNo(String firstPhNo) {
		this.firstPhNo = firstPhNo;
	}
	public String getSecondPhNo() {
		return secondPhNo;
	}
	public void setSecondPhNo(String secondPhNo) {
		this.secondPhNo = secondPhNo;
	}
	
}
