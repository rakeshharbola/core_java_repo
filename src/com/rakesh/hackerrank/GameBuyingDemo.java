package com.rakesh.hackerrank;

import java.util.ArrayList;
import java.util.List;

public class GameBuyingDemo {
	
	public static int howManyGames(int p, int d, int m, int s) {
		int totalGames = 0;
		List<Integer> list = new ArrayList<>();
		int sum=0;
		do {
			if(p<=s) {
			if(p<=m)
				p=m;
			System.out.println("p = "+p);
			sum=sum+p;
			s = s-p;
			p = p-d;
			totalGames++;
			}
		}while(s>=m &&  p<=s);
		return totalGames;
	}
	public static void main(String[] args) {
		int result = GameBuyingDemo.howManyGames(20,  3,  6, 85);
		System.out.println(result);
	}

}
