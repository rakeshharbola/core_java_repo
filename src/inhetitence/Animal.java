package inhetitence;

public class Animal {

	protected void show() {
		System.out.println("This is the show method of Animal class.");
	}
	
	protected void eat() {
		System.out.println("This is the eat method of Animal class.");
	}
	
	protected void move() {
		System.out.println("This is the move method of Animal class.");
	}
	
	protected void exceptionTestMethod() {
		System.out.println("ExceptionTestMethod of Animal class.");
	}
	
	protected void exceptionOverrideFailureMethod() {
		System.out.println("This method will throw error while will override this along with checked exception throwing.");
	}
}
