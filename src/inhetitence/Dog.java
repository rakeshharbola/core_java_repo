package inhetitence;

import java.io.IOException;

public class Dog extends Animal{

/*	@Override
	protected void show() {
		System.out.println("This is the show method of Dog class.");
	}*/ 
	
	@Override
	protected void eat() {
		System.out.println("This is the eat method of Dog class.");
	}
	
	@Override 
	protected void move() {
		System.out.println("This is the move method of Dog class.");
	}
	
	@Override
	public void exceptionTestMethod() throws NullPointerException {
		System.out.println("This is ExceptionTestMethod of Dog class.");
	}
	
/*	@Override
	public void exceptionOverrideFailureMethod() throws IOException {
		System.out.println("This is throwing error as it's parent class is not thowing any error.");
	}*/
	
}
