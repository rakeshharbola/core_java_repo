package inhetitence;

class Pen {
	private String type;
	private String color;
	
	public Pen(String type, String color) {
		this.type=type;
		this.color=color;
	}

	public String getType() {
		return type;
	}

	public String getColor() {
		return color;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	String getDetails() {
		return "Pen of type "+this.type+" is of "+this.color+" color";
	}
}

class BallPen extends Pen{
	String type="Ball";
	String color = "Black";
	
	public BallPen(String type, String color) {
		super(type, color);
			this.type = this.getType();
			this.color = this.getColor();
			// TODO Auto-generated constructor stub
		}

	String getDetails() {
		return "This is overridden method with type "+this.getType()+" or color "+this.getColor();
	}
}

public class InheritenceDemo {

	public static void main(String[] args) {
		Pen ballPen = new BallPen("Ball", "Blue");
		Pen pen = new Pen("Pen", "Red");
		
		System.out.println(ballPen.getDetails());
		System.out.println(pen.getDetails());

	}

}
